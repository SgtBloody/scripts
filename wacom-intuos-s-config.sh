#!/bin/sh
set -eu

xsetwacom set "Wacom Intuos S Pen stylus" MapToOutput 1728x1080+0x0
xsetwacom set "Wacom Intuos S Pen stylus" MapToOutput HEAD-0

exit 0
